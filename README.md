# Python - From Data Acquistion to Publication

Materials for an online workshop demonstrating the use of Python in the laboratory for data acquisition, analysis, and presentation.

The notebook was initially used as part of the Royal Australian Chemical Institute (RACI) Physical Chemistry Division's online workshop series on 29th June 2021. A video recording of the event is available on [YouTube](https://youtu.be/v0R_Hiwj2Yk)

The works are licensed under a Creative Commons CC BY-SA 4.0 license, and are free to use or distribute.
